#!/bin/bash
# Generate the bookmarklet files

# Variables
MINIFY=$(command -v uglifyjs)
BACKUP_UGLIFY="./node_modules/uglify-js/bin/uglifyjs"
MINIFY_ARGS="-m -c"
SRC="pinboard-particular.js"
OUT="build"

if [ -z "$MINIFY" ]; then
    if [ -x "$BACKUP_UGLIFY" ]; then
        MINIFY="$BACKUP_UGLIFY"
    else
        printf "No uglifyjs found. Bailing out\n"
        exit 1
    fi
fi

if [ ! -d $OUT ]; then
	mkdir $OUT
fi

# create 'classic' bookmarklet
printf "javascript:%s" "$($MINIFY $SRC $MINIFY_ARGS)" > $OUT/bookmark.js

# create 'read later' bookmarklet
printf "javascript:%s" > $OUT/readlater.js
sed 's/readlater = false/readlater = true/' $SRC |
	$MINIFY $MINIFY_ARGS 2>/dev/null >> $OUT/readlater.js

# create 'pinswift' bookmarklet
printf "javascript:%s" >$OUT/pinswift.js
sed 's/appUrl = null/appUrl = "pinswift:\/\/x-callback-url\/add?"/' $SRC |
	$MINIFY $MINIFY_ARGS 2>/dev/null >> $OUT/pinswift.js
